var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json');
  var msg = 'sample response';
  // for express, we could use res.json
  res.json({message: msg});
  //res.end(JSON.stringify({message: msg}));
});

module.exports = router;
